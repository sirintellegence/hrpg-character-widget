HabitRPG Widget Plugin for Wordpress
======

This project was designed to be used as a widget on your website to show your [HabitRPG](https://habitrpg.com) character and some stats on your own personal website. 

* [Official Page ](https://gitlab.com/crystalraebryant/habitrpg-widget)
* To install -- download zip in the /dist directory and upload the plugin to your Wordpress site.

The future! 
======

* Package up into a zip, possibly put in the Wordpress plugin repository. Right now, with all the image files, it's HUGE. 
* Turn into a shortcode so that the widget isn't limited in where you put it and so that you can show multiple sprites on one site.
* Make sure ALL sprite image pieces are updated. Possibly if switching to the [sprite maps](https://habitica.com/common/dist/sprites/spritesmith-main-8-d4077308.png) Habitica uses would help limit size. 
